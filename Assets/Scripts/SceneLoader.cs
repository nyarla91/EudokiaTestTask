﻿using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] private int _mainMenuReference;
    [SerializeField] private int _gameplayReference;
    [SerializeField] private float _transitionDuration;
    [SerializeField] private CanvasGroup _blackScreen; 

    private bool _loadingScene;
    
    public void LoadMainMenu() => LoadScene(_mainMenuReference);
    public void LoadGameplay() => LoadScene(_gameplayReference);

    private void LoadScene(int scene)
    {
        StartCoroutine(LoadingScene(scene));
    }
    
    private IEnumerator LoadingScene(int scene)
    {
        if (_loadingScene)
            yield break;
        _loadingScene = true;

        _blackScreen.DOKill();
        _blackScreen.DOFade(1, _transitionDuration);
        yield return new WaitForSeconds(_transitionDuration);

        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(scene);
        yield return new WaitUntil(() => asyncOperation.isDone);
        
        _blackScreen.DOKill();
        _blackScreen.DOFade(0, _transitionDuration);
        yield return new WaitForSeconds(_transitionDuration);
        
        _loadingScene = false;
    }
}