﻿using System;
using System.Collections.Generic;
using Extentions;
using UnityEngine;
using Zenject;

namespace Factory
{
    public class PoolFactory : Transformable
    {
        [SerializeField] private GameObject _prefab;
        [SerializeField] private List<PoolObject> _pool = new List<PoolObject>();
        
        [Inject] private ContainerFactory ContainerFactory { get; set; }

        public void DisableObject(PoolObject objectToRemove)
        {
            objectToRemove.gameObject.SetActive(false);
            objectToRemove.Transform.SetParent(Transform);
        }

        public PoolObject SpawnObject(Vector3 position, Transform parent = null)
            =>  SpawnObject<PoolObject>(position, null, parent);

        public PoolObject SpawnObject(Vector3 position, GameObject overridePrefab, Transform parent = null)
            =>  SpawnObject<PoolObject>(position, overridePrefab, parent);

        public T SpawnObject<T>(Vector3 position, Transform parent = null) where T : Component
            =>  SpawnObject<T>(position, null, parent);
        
        public T SpawnObject<T>(Vector3 position, GameObject overridePrefab, Transform parent = null) where T : Component
        {
            GameObject prefab = overridePrefab ?? _prefab;
            
            PoolObject newObject;
            T component;
            
            for (int i = 0; i < _pool.Count; i++)
            {
                if (_pool[i].gameObject.activeSelf || ! _pool[i].TryGetComponent(out component))
                    continue;
                newObject = _pool[i];
                newObject.gameObject.SetActive(true);
                newObject.Transform.position = position;
                newObject.Transform.SetParent(parent);
                newObject.OnPoolEnable();
                return component;
            }
            component = InstantiatePrefab<T>(position, prefab, parent, out newObject);
            newObject.OnPoolEnable();
            return component;
        }

        private T InstantiatePrefab<T>(Vector3 position, GameObject prefab, Transform parent, out PoolObject poolObject) where T : Component
        {
            T instance = ContainerFactory.Instantiate<T>(prefab, position, parent);
            if (instance == null)
                throw new NullReferenceException($"{prefab} prefab does not have component of {typeof(T)} type");
            if ( ! instance.TryGetComponent(out poolObject))
                throw new NullReferenceException($"{prefab} prefab must contain PoolObject component");
            poolObject.Transform.position = position;
            poolObject.Transform.parent = parent;
            poolObject.PoolInit(this);
            instance.gameObject.SetActive(true);
            _pool.Add(poolObject);
            return instance;
        }

        private void OnValidate()
        {
            if (_prefab == null || _prefab.TryGetComponent(out PoolObject _))
                return;
            _prefab = null;
            Debug.LogWarning("PoolFactory prefab must contain PoolObject component");
        }
    }
}