// GENERATED AUTOMATICALLY FROM 'Assets/Scripts/Gameplay/Input/MenuActions.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @MenuActions : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @MenuActions()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""MenuActions"",
    ""maps"": [
        {
            ""name"": ""All"",
            ""id"": ""6cdbe4b3-be3c-4a18-b701-6de1d7dde86f"",
            ""actions"": [
                {
                    ""name"": ""ToggleMenu"",
                    ""type"": ""Button"",
                    ""id"": ""569c246e-5e68-4a5a-b620-f9651ce312d7"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""359de6ad-accf-427f-91eb-150742c9fb07"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ToggleMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // All
        m_All = asset.FindActionMap("All", throwIfNotFound: true);
        m_All_ToggleMenu = m_All.FindAction("ToggleMenu", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // All
    private readonly InputActionMap m_All;
    private IAllActions m_AllActionsCallbackInterface;
    private readonly InputAction m_All_ToggleMenu;
    public struct AllActions
    {
        private @MenuActions m_Wrapper;
        public AllActions(@MenuActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @ToggleMenu => m_Wrapper.m_All_ToggleMenu;
        public InputActionMap Get() { return m_Wrapper.m_All; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(AllActions set) { return set.Get(); }
        public void SetCallbacks(IAllActions instance)
        {
            if (m_Wrapper.m_AllActionsCallbackInterface != null)
            {
                @ToggleMenu.started -= m_Wrapper.m_AllActionsCallbackInterface.OnToggleMenu;
                @ToggleMenu.performed -= m_Wrapper.m_AllActionsCallbackInterface.OnToggleMenu;
                @ToggleMenu.canceled -= m_Wrapper.m_AllActionsCallbackInterface.OnToggleMenu;
            }
            m_Wrapper.m_AllActionsCallbackInterface = instance;
            if (instance != null)
            {
                @ToggleMenu.started += instance.OnToggleMenu;
                @ToggleMenu.performed += instance.OnToggleMenu;
                @ToggleMenu.canceled += instance.OnToggleMenu;
            }
        }
    }
    public AllActions @All => new AllActions(this);
    public interface IAllActions
    {
        void OnToggleMenu(InputAction.CallbackContext context);
    }
}
