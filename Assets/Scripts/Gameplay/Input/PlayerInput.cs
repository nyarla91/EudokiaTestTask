﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Gameplay.Input
{
    public class PlayerInput : MonoBehaviour
    {
        private InputActions _actions;

        public Vector2 MousePosition => _actions.All.MousePosition.ReadValue<Vector2>();

        public event Action ShootPressed;
        
        private void Awake()
        {
            _actions = new InputActions();
            _actions.Enable();
            _actions.All.Shoot.started += PressShoot;
        }
        
        private void PressShoot(InputAction.CallbackContext _) => ShootPressed?.Invoke();

        private void OnDestroy()
        {
            _actions.All.Shoot.started -= PressShoot;
        }
    }
}