﻿using System;
using Gameplay.Enemy;
using Gameplay.Pause;
using Gameplay.UI;
using UnityEngine;
using Zenject;

namespace Gameplay
{
    public class Defeat : MonoBehaviour
    {
        [SerializeField] private int _threshold;
        [SerializeField] private EnemySpawner _spawner;
        [SerializeField] private InGameMenu _menu;
        [SerializeField] private GameObject _gameOverMessage;

        public int Threshold => _threshold;
        
        [Inject] private IPauseSetService PauseSet { get; set; }

        public event Action Defeated;
        
        private void Awake()
        {
            _spawner.EnemySpawned += TryEndGame;
        }

        private void TryEndGame()
        {
            if (_spawner.EnemiesAlive < _threshold)
                return;
            
            _spawner.EnemySpawned -= TryEndGame;
            PauseSet.PauseFromSource(this);
            _menu.ToggleMenu();
            _gameOverMessage.SetActive(true);
            Defeated?.Invoke();
        }
    }
}