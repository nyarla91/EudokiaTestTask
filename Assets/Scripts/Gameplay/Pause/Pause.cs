﻿using System.Collections.Generic;
using UnityEngine;

namespace Gameplay.Pause
{
    public class Pause : MonoBehaviour, IPauseSetService, IPauseReadService
    {
        private readonly List<MonoBehaviour> _pauseSources = new List<MonoBehaviour>();

        public bool IsPaused => _pauseSources.Count > 0;
        public bool IsUnpaused => ! IsPaused;
        
        public void PauseFromSource(MonoBehaviour source)
        {
            if ( ! _pauseSources.Contains(source))
                _pauseSources.Add(source);
        }

        public void UnpauseFromSource(MonoBehaviour source)
        {
            if (_pauseSources.Contains(source))
                _pauseSources.Remove(source);
        }
    }
}