﻿namespace Gameplay.Pause
{
    public interface IPauseReadService
    {
        public bool IsPaused { get; }
        public bool IsUnpaused { get; }
    }
}