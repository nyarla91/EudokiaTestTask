﻿using UnityEngine;

namespace Gameplay.Pause
{
    public interface IPauseSetService
    {
        void PauseFromSource(MonoBehaviour source);

        void UnpauseFromSource(MonoBehaviour source);
    }
}