﻿using System;
using Extentions;
using Gameplay.Enemy;
using TMPro;
using UnityEngine;

namespace Gameplay.UI
{
    public class EnemyCounter : MonoBehaviour
    {
        [SerializeField] private TMP_Text _counter;
        [SerializeField] private EnemySpawner _enemySpawner;
        [SerializeField] private Defeat _defeat;
        [SerializeField] private Range _fontSize;

        private void Update()
        {
            _counter.text = $"{_enemySpawner.EnemiesAlive} / {_defeat.Threshold}";
            float fontT = (float) _enemySpawner.EnemiesAlive / _defeat.Threshold;
            _counter.fontSize = Mathf.Lerp(_fontSize.Min, _fontSize.Max, fontT);
        }
    }
}