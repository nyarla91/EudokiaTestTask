﻿using TMPro;
using UnityEngine;

namespace Gameplay.UI
{
    public class ScoreCounter : MonoBehaviour
    {
        [SerializeField] private Score _score;
        [SerializeField] private TMP_Text _counter;

        private void Update()
        {
            _counter.text = _score.CurrentScore.ToString();
        }
    }
}