﻿using Gameplay.Pause;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

namespace Gameplay.UI
{
    public class InGameMenu : MonoBehaviour
    {
        [SerializeField] private CanvasGroup _canvasGroup;
        
        private bool _openned;
        private MenuActions _menuActions;
        
        [Inject] private SceneLoader SceneLoader { get; set; }
        [Inject] private IPauseSetService PauseSet { get; set; }

        public void Restart() => SceneLoader.LoadGameplay();
        
        public void QuitToMenu() => SceneLoader.LoadMainMenu();
        
        public void QuitToDesktop() => Application.Quit();

        private void Awake()
        {
            _menuActions = new MenuActions();
            _menuActions.Enable();
            _menuActions.All.ToggleMenu.started += ToggleMenu;
        }

        private void ToggleMenu(InputAction.CallbackContext _) => ToggleMenu();
        
        public void ToggleMenu()
        {
            _openned = ! _openned;
            
            if (_openned)
                PauseSet.PauseFromSource(this);
            else
                PauseSet.UnpauseFromSource(this);

            _canvasGroup.blocksRaycasts = _openned;
            _canvasGroup.alpha = _openned ? 1 : 0;
        }

        private void OnDestroy()
        {
            _menuActions.All.ToggleMenu.started -= ToggleMenu;
        }
    }
}