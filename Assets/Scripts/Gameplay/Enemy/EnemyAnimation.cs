﻿using System;
using Extentions;
using Gameplay.Pause;
using UnityEngine;
using Zenject;

namespace Gameplay.Enemy
{
    public class EnemyAnimation : MonoBehaviour
    {
        [SerializeField] private Animator _animator;
        [SerializeField] private Transform _model;
        [SerializeField] [Range(0, 0.5f)] private float _rotationSpeed;

        public event Func<Vector3> VelocityBind;

        [Inject] private IPauseReadService PauseRead { get; set; }
        
        private void Update()
        {
            _animator.SetFloat("moveSpeed", PauseRead.IsUnpaused ? (VelocityBind.GetValidatedValue().magnitude) : 0);
        }

        private void FixedUpdate()
        {
            Vector3 currentLook = _model.forward;
            Vector3 targetLook = VelocityBind.GetValidatedValue().normalized;
            float maxDegreesDelta = Vector3.Angle(currentLook, targetLook) * _rotationSpeed;
            _model.rotation = Quaternion.RotateTowards
                (_model.rotation, Quaternion.LookRotation(targetLook), maxDegreesDelta);
        }
    }
}