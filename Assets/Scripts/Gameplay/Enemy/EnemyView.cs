﻿using System;
using Extentions;
using UnityEngine;

namespace Gameplay.Enemy
{
    public class EnemyView : MonoBehaviour
    {
        [SerializeField] private Renderer _renderer;
        [SerializeField] private Gradient _healthColor;

        public event Func<float> HealthPercentBind;

        private void Update()
        {
            _renderer.material.color = _healthColor.Evaluate(HealthPercentBind.GetValidatedValue());
        }
    }
}