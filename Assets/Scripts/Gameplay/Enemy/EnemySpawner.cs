﻿using System;
using Extentions;
using Factory;
using Gameplay.Pause;
using UnityEngine;
using Zenject;

namespace Gameplay.Enemy
{
    public class EnemySpawner : MonoBehaviour
    {
        [SerializeField] private PoolFactory _factory;
        [SerializeField] private BoxCollider _spawnArea;
        [SerializeField] private IncrementalValue _spawnRate;
        [SerializeField] private Range _spawnRateRandom;
        [SerializeField] private IncrementalValue _enemySpeed;
        [SerializeField] private IncrementalValue _enemyhealth;

        private Timer _increaseTimer;
        private Timer _spawnTimer;
        private Timer _freezeTimer;
        public int EnemiesAlive { get; private set; }

        public float SpawnPeriod => _spawnRate.InvertedValue * _spawnRateRandom.Random;

        [Inject] private IPauseReadService PauseRead { get; set; }

        public event Action EnemySpawned;

        private void Awake()
        {
            _increaseTimer = new Timer(this,1, PauseRead, true).Start();
            _increaseTimer.Expired += IncreaseStats;
            
            _spawnTimer = new Timer(this, SpawnPeriod, PauseRead, true).Start();
            _spawnTimer.Expired += SpawnEnemy;
            
            _freezeTimer = new Timer(this, 1, PauseRead, true).Start();
            _freezeTimer.Expired += Unfreeze;
        }

        public void Freeze(float duration)
        {
            _spawnTimer.Stop();
            _freezeTimer.Length = duration;
            _freezeTimer.Restart();
        }

        private void Unfreeze()
        {
            _spawnTimer.Start();
        }

        private void SpawnEnemy()
        {
            Vector3 position = _spawnArea.bounds.RandomPointInBounds().WithY(0);
            PoolObject enemy = _factory.SpawnObject(position);
            enemy.GetComponent<EnemyMovement>().Init(_enemySpeed.Value);
            EnemyStatus enemyStatus = enemy.GetComponent<EnemyStatus>();
            enemyStatus.Init(_enemyhealth.Value);
            enemyStatus.Died += RemoveEnemyAlive;
            EnemiesAlive++;
            EnemySpawned?.Invoke();

            _spawnTimer.Length = SpawnPeriod;
        }
        private void RemoveEnemyAlive(EnemyStatus enemyStatus)
        {
            EnemiesAlive--;
            enemyStatus.Died -= RemoveEnemyAlive;
        }

        private void IncreaseStats()
        {
            _spawnRate.Increase();
            _enemySpeed.Increase();
            _enemyhealth.Increase();
        }
    }
}