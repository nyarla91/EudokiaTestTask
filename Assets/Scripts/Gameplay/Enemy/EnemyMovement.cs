﻿using Extentions;
using Gameplay.Pause;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

namespace Gameplay.Enemy
{
    [RequireComponent(typeof(Rigidbody))]
    public class EnemyMovement : MonoBehaviour
    {
        public Vector3 Velocity { get; private set; }

        private Rigidbody _rigidbody;

        private Rigidbody Rigidbody => _rigidbody ??= GetComponent<Rigidbody>();

        [Inject] private IPauseReadService PauseRead { get; set; }
        
        public void Init(float speed)
        {
            Vector3 direction = Random.insideUnitCircle.XYtoXZ();
            Velocity = direction * speed;
        }

        private void FixedUpdate()
        {
            Rigidbody.velocity = PauseRead.IsPaused ? Vector3.zero : Velocity;
        }

        private void OnCollisionEnter(Collision other)
        {
            Velocity = Vector3.Reflect(Velocity, other.contacts[0].normal);
        }
    }
}