﻿using System;
using Factory;
using UnityEngine;

namespace Gameplay.Enemy
{
    [RequireComponent(typeof(PoolObject))]
    public class EnemyStatus : MonoBehaviour, IDamageTaker
    {
        private float _maxHealth; 
        private float _health;

        public float HealthPercent => _health / _maxHealth;

        private PoolObject _poolObject;

        private PoolObject PoolObject => _poolObject ??= GetComponent<PoolObject>();

        public event Action<EnemyStatus> Died;
        
        public void Init(float health)
        {
            _maxHealth = _health = health;
        }
        
        public void TakeDamage(float damage)
        {
            if (damage <= 0)
                return;
            _health -= Mathf.Max(damage, 0);
            if (!(_health <= 0))
                return;
            PoolObject.PoolDisable();
            Died?.Invoke(this);
        }
    }
}