﻿using UnityEngine;

namespace Gameplay.Enemy
{
    public class EnemyPresenter : MonoBehaviour
    {
        [SerializeField] private EnemyAnimation _animation;
        [SerializeField] private EnemyView _view;
        [SerializeField] private EnemyMovement _movement;
        [SerializeField] private EnemyStatus _status;

        private void Awake()
        {
            _animation.VelocityBind += () => _movement.Velocity;
            _view.HealthPercentBind += () => _status.HealthPercent;
        }
    }
}