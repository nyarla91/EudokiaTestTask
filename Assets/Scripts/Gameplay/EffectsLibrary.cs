﻿using Factory;
using UnityEngine;

namespace Gameplay
{
    public class EffectsLibrary : MonoBehaviour
    {
        [SerializeField] private PoolFactory _explosionFactory;

        public PoolFactory ExplosionFactory => _explosionFactory;
    }
}