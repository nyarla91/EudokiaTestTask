﻿using Extentions;
using Gameplay.Pause;
using Highscore;
using UnityEngine;
using Zenject;

namespace Gameplay
{
    public class Score : MonoBehaviour
    {
        [SerializeField] private Defeat _defeat;
        [SerializeField] private float _pointsPerSecond;

        private Timer _period;
        
        public int CurrentScore { get; private set; }

        [Inject] private IHighscoreRecordingAddService HighscoreTable { get; set; }
        [Inject] private IPauseReadService PauseRead { get; set; }

        private void Awake()
        {
            _period = new Timer(this, 1 / _pointsPerSecond, PauseRead, true).Start();
            _period.Expired += () => CurrentScore++;
            
            _defeat.Defeated += OnDefeat;
        }

        private void OnDefeat()
        {
            _period.Stop();
            HighscoreTable.AddRecording(CurrentScore);
        }
    }
}