﻿using System;
using Extentions;
using Factory;
using Gameplay.Pause;
using UnityEngine;
using Zenject;

namespace Gameplay
{
    [RequireComponent(typeof(PoolObject))]
    public class ExplosionEffect : Transformable
    {
        [SerializeField] private ParticleSystem _particleSystem;
        [SerializeField] private float _scalePerRadiusUnit;
        [SerializeField] private float _lifetime;

        private Timer _life;
        
        private PoolObject _poolObject;

        private PoolObject PoolObject => _poolObject ??= GetComponent<PoolObject>();
        
        [Inject] private IPauseReadService PauseRead { get; set; }
        
        public void Init(float radius)
        {
            float scale = _scalePerRadiusUnit * radius;
            Transform.localScale = Vector3.one * scale;
            _life = new Timer(this, _lifetime, PauseRead).Start();
            _life.Expired += Dispose;
            _particleSystem.Play();
        }

        private void Update()
        {
            ParticleSystem.MainModule module = _particleSystem.main;
            module.simulationSpeed = PauseRead.IsUnpaused ? 1 : 0;
        }

        private void Dispose()
        {
            PoolObject.PoolDisable();
        }
    }
}