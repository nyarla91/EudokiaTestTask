﻿using UnityEngine;

namespace Gameplay
{
    public interface IDamageTaker
    {
        public void TakeDamage(float damage);
    }
}