﻿using System;
using System.Linq;
using Extentions;
using Factory;
using UnityEngine;

namespace Gameplay
{
    [Serializable]
    public struct AreaDamage
    {
        [SerializeField] private float _damage;
        [SerializeField] private float _radius;
        [SerializeField] private AnimationCurve _falloff;

        public void DealDamage(GameObject source, Vector3 origin, PoolFactory effectFactory, float damageMultiplier = 1)
        {
            Collider[] targets = Physics.OverlapSphere(origin, _radius);
            
            foreach (Collider target in targets.Where(target => target.gameObject != source))
            {
                if ( ! target.TryGetComponent(out IDamageTaker damageTaker))
                    continue;
                
                float distance = Vector3.Distance(origin, target.transform.position);
                float totalDamage = _damage * damageMultiplier * (1 - _falloff.Evaluate(distance / _radius));
                damageTaker.TakeDamage(totalDamage);
            }

            ExplosionEffect effect = effectFactory.SpawnObject<ExplosionEffect>(origin.WithY(0.5f));
            effect.Init(_radius);
        }
    }
}