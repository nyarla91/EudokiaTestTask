﻿using Extentions;
using Factory;
using Gameplay.Pause;
using UnityEngine;
using Zenject;

namespace Gameplay.Weapon
{
    public class WeaponAttack : Transformable, IWeaponUpgrade
    {
        [SerializeField] private PoolFactory _projectileFactory;
        [SerializeField] private Transform _projectileOrigin;
        [SerializeField] private IncrementalValue _shootRate;
        [SerializeField] private IncrementalValue _damageMultiplier;

        private Timer _cooldown;
        
        public bool ShotReady => _cooldown.IsExpired;
        
        [Inject] private IPauseReadService PauseRead { get; set; }

        public void TryShoot()
        {
            if ( ! ShotReady)
                return;
            Shoot();
            _cooldown.Restart();
        }

        public void UpgradeAttackDamage() => _damageMultiplier.Increase();
        
        public void UpgradeAttackSpeed() => _shootRate.Increase();

        private void Awake()
        {
            _cooldown = new Timer(this, _shootRate.InvertedValue, PauseRead).Start();
        }

        private void Update()
        {
            _cooldown.Length = _shootRate.InvertedValue;
        }

        private void Shoot()
        {
            Projectile projectile = _projectileFactory.SpawnObject<Projectile>(_projectileOrigin.position);
            projectile.Init(Transform.forward);
        }
    }
}