﻿using Gameplay.Input;
using UnityEngine;

namespace Gameplay.Weapon
{
    public class PlayerPresenter : MonoBehaviour
    {
        [SerializeField] private PlayerInput _input;
        [SerializeField] private WeaponView _view;
        [SerializeField] private WeaponSight _sight;
        [SerializeField] private WeaponAttack _attack;

        private void Awake()
        {
            _view.ShowRocketBind += () => _attack.ShotReady;
            _sight.MousePositionBind += () => _input.MousePosition;
            _input.ShootPressed += _attack.TryShoot;
        }
    }
}