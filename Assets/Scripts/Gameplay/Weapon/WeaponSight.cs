﻿using System;
using Extentions;
using Gameplay.Pause;
using UnityEngine;
using Zenject;

namespace Gameplay.Weapon
{
    public class WeaponSight : Transformable
    {
        private Camera _mainCamera;

        private Camera MainCamera => _mainCamera ??= Camera.main;

        [Inject] private IPauseReadService PauseRead { get; set; }
         
        public event Func<Vector2> MousePositionBind; 
        
        private void Update()
        {
            if (PauseRead.IsPaused)
                return;
            
            Ray ray = MainCamera.ScreenPointToRay(MousePositionBind.GetValidatedValue());
            int layerMask = LayerMask.GetMask("MouseArea");
            if ( ! Physics.Raycast(ray, out RaycastHit raycast, Single.MaxValue, layerMask))
                return;
            
            Transform.rotation = Quaternion.LookRotation(raycast.point - Transform.position);
        }
    }
}