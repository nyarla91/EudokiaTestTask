﻿using Extentions;
using Factory;
using Gameplay.Pause;
using UnityEngine;
using Zenject;

namespace Gameplay.Weapon
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(PoolObject))]
    public class Projectile : Transformable 
    {
        [SerializeField] private float _speed;
        [SerializeField] private AreaDamage _areaDamage;

        private Vector3 _direction;
        private float _damageMultiplier;
        
        private Rigidbody _rigidbody;
        private PoolObject _poolObject;

        private Rigidbody Rigidbody => _rigidbody ??= GetComponent<Rigidbody>();
        public PoolObject PoolObject => _poolObject ??= GetComponent<PoolObject>();
        
        [Inject] private IPauseReadService PauseRead { get; set; }
        [Inject] private EffectsLibrary EffectsLibrary { get; set; }
        
        public void Init(Vector3 direction, float damageMultiplier = 1)
        {
            _damageMultiplier = damageMultiplier;
            Transform.rotation = Quaternion.LookRotation(direction);
            _direction = direction;
        }

        private void OnTriggerEnter(Collider other)
        {
            _areaDamage.DealDamage(gameObject, Transform.position, EffectsLibrary.ExplosionFactory, _damageMultiplier);
            PoolObject.PoolDisable();
        }

        private void Awake()
        {
            PoolObject.PoolDisabled += _ => Rigidbody.angularVelocity = Vector3.zero;
        }

        private void FixedUpdate()
        {
            Rigidbody.velocity = PauseRead.IsPaused ? Vector3.zero : (_direction * _speed);
        }
    }
}