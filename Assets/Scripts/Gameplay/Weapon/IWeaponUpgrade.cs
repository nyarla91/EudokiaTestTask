﻿namespace Gameplay.Weapon
{
    public interface IWeaponUpgrade
    {
        void UpgradeAttackDamage();
        
        void UpgradeAttackSpeed();
    }
}