﻿using System;
using Extentions;
using UnityEngine;

namespace Gameplay.Weapon
{
    public class WeaponView : MonoBehaviour
    {
        [SerializeField] private GameObject _rocketView;

        public event Func<bool> ShowRocketBind;

        private void Update()
        {
            _rocketView.SetActive(ShowRocketBind.GetValidatedValue());
        }
    }
}