﻿using Gameplay.Weapon;
using Zenject;

namespace Gameplay.PowerUps
{
    public class AttackSpeedPowerUp : PowerUp
    {
        [Inject] private IWeaponUpgrade WeaponAttack { get; set; }
        
        protected override void OnCollect()
        {
            WeaponAttack.UpgradeAttackSpeed();
        }
    }
}