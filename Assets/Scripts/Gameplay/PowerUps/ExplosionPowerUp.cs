﻿using UnityEngine;
using Zenject;

namespace Gameplay.PowerUps
{
    public class ExplosionPowerUp : PowerUp
    {
        [SerializeField] private AreaDamage _areaDamage;
        
        [Inject] private EffectsLibrary EffectsLibrary { get; set; }
        
        protected override void OnCollect()
        {
            _areaDamage.DealDamage(gameObject, Transform.position, EffectsLibrary.ExplosionFactory);
        }
    }
}