﻿using Gameplay.Enemy;
using UnityEngine;
using Zenject;

namespace Gameplay.PowerUps
{
    public class FreezePowerUp : PowerUp
    {
        [SerializeField] private float _duration;

        [field: Inject] private EnemySpawner Spawner { get; set; }

        protected override void OnCollect()
        {
            Spawner.Freeze(_duration);
        }
    }
}