﻿using Extentions;
using Factory;
using UnityEngine;

namespace Gameplay.PowerUps
{
    [RequireComponent(typeof(PoolObject))]
    public abstract class PowerUp : Transformable, IDamageTaker
    {
        private PoolObject _poolObject;

        private PoolObject PoolObject => _poolObject ??= GetComponent<PoolObject>();
        
        public void TakeDamage(float damage)
        {
            PoolObject.PoolDisable();
            OnCollect();
        }

        protected abstract void OnCollect();
    }
}