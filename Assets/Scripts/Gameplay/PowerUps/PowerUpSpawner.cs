﻿using Extentions;
using Factory;
using Gameplay.Pause;
using UnityEngine;
using Zenject;

namespace Gameplay.PowerUps
{
    public class PowerUpSpawner : MonoBehaviour
    {
        [SerializeField] private PoolFactory[] _factories;
        [SerializeField] private BoxCollider _spawnArea;
        [SerializeField] private Range _spawnPeriod;

        private Timer _spawnTimer;
        
        [Inject] private IPauseReadService PauseRead { get; set; }
        
        private void Start()
        {
            _spawnTimer = new Timer(this, _spawnPeriod.Random, PauseRead, true).Start();
            _spawnTimer.Expired += SpawnPowerUp;
        }

        private void SpawnPowerUp()
        {
            PoolFactory type = _factories[Random.Range(0, _factories.Length)];
            Vector3 position = _spawnArea.bounds.RandomPointInBounds().WithY(0);
            type.SpawnObject(position);

            _spawnTimer.Length = _spawnPeriod.Random;
        }
    }
}