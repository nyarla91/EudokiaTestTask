﻿using Gameplay.Weapon;
using Zenject;

namespace Gameplay.PowerUps
{
    public class AttackDamagePowerUp : PowerUp
    {
        [Inject] private IWeaponUpgrade WeaponAttack { get; set; }
        
        protected override void OnCollect()
        {
            WeaponAttack.UpgradeAttackDamage();
        }
    }
}