﻿using UnityEngine;
using Zenject;

public class MainMenu : MonoBehaviour
{
    [Inject] private SceneLoader SceneLoader { get; set; }
        
    public void StartGame() => SceneLoader.LoadGameplay();
        
    public void QuitToDesktop() => Application.Quit();
}