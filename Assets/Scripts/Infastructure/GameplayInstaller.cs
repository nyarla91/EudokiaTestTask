﻿using Gameplay;
using Gameplay.Enemy;
using Gameplay.Pause;
using Gameplay.Weapon;
using UnityEngine;
using Zenject;

namespace Infastructure
{
    public class GameplayInstaller : MonoInstaller
    {
        [SerializeField] private EnemySpawner _enemySpawner;
        [SerializeField] private GameObject _playerPrefab;
        [SerializeField] private Transform _playerOrigin;
        [SerializeField] private GameObject _pausePrefab;
        [SerializeField] private EffectsLibrary _effectsLibrary;
        
        public override void InstallBindings()
        {
            Container.Bind<EnemySpawner>().FromInstance(_enemySpawner).AsSingle();
            Container.Bind<EffectsLibrary>().FromInstance(_effectsLibrary).AsSingle();
            BindPause();
            BindPlayer();
        }

        private void BindPause()
        {
            Pause pause = Container.InstantiatePrefab(_pausePrefab, transform).GetComponent<Pause>();
            Container.Bind<IPauseSetService>().FromInstance(pause).AsSingle();
            Container.Bind<IPauseReadService>().FromInstance(pause).AsSingle();
        }

        private void BindPlayer()
        {
            GameObject player = Container.InstantiatePrefab(_playerPrefab, _playerOrigin);
            Container.Bind<IWeaponUpgrade>().FromInstance(player.GetComponent<WeaponAttack>()).AsSingle();
        }
    }
}