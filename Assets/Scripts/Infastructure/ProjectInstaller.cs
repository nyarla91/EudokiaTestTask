﻿using Highscore;
using UnityEngine;
using Zenject;

namespace Infastructure
{
    public class ProjectInstaller : MonoInstaller
    {
        [SerializeField] private GameObject _sceneLoaderPrefab;
        [SerializeField] private GameObject _highscoreTablePrefab;

        public override void InstallBindings()
        {
            GameObject sceneLoader = Container.InstantiatePrefab(_sceneLoaderPrefab, transform);
            Container.Bind<SceneLoader>().FromInstance(sceneLoader.GetComponent<SceneLoader>()).AsSingle();
            BindHighscoreTable();
        }

        private void BindHighscoreTable()
        {
            HighscoreTable highscoreTable = Container.InstantiatePrefab(_highscoreTablePrefab, transform).GetComponent<HighscoreTable>();
            Container.Bind<IHighscoreRecordingAddService>().FromInstance(highscoreTable).AsSingle();
            Container.Bind<IHighscorePlayerNameSetService>().FromInstance(highscoreTable).AsSingle();
            Container.Bind<IHighscoreTableReadService>().FromInstance(highscoreTable).AsSingle();
        }
    }
}