﻿using System.Collections.Generic;

namespace Highscore
{
    public interface IHighscoreTableReadService
    {
        IEnumerable<ScoreRecording> Table { get; }
    }
}