﻿namespace Highscore
{
    public interface IHighscorePlayerNameSetService
    {
        string PlayerName { set; }
    }
}