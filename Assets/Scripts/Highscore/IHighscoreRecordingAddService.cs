﻿namespace Highscore
{
    public interface IHighscoreRecordingAddService
    {
        void AddRecording(int score);
    }
}