﻿using TMPro;
using UnityEngine;

namespace Highscore
{
    public class ScoreRecordingView : MonoBehaviour
    {
        [SerializeField] private TMP_Text _playerName;
        [SerializeField] private TMP_Text _score;

        public void Init(string playerName, int score)
        {
            _playerName.text = playerName;
            _score.text = score.ToString();
        }
    }
}