﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace Highscore
{
    public class HighscoreTable : MonoBehaviour, IHighscorePlayerNameSetService, IHighscoreRecordingAddService, IHighscoreTableReadService
    {
        [SerializeField] private int _recordingsCapacity;

        [SerializeField] private List<ScoreRecording> _table = new List<ScoreRecording>();

        public IEnumerable<ScoreRecording> Table => _table;
        
        private string SaveFilePath => $"{Application.dataPath}/Highscore.save";

        public string PlayerName { private get; set; }


        public void AddRecording(int score)
        {
            _table.Add(new ScoreRecording(PlayerName, score));
            _table = _table.OrderBy(recording => -recording.Score).ToList();
            
            while (_table.Count > _recordingsCapacity)
                _table.RemoveAt(_table.Count - 1);
            
            Save();
        }

        private void Save()
        {
            FileStream stream = new FileStream(SaveFilePath, FileMode.Create);
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, _table);
            stream.Close();
        }

        private void Load() 
        {
            if ( ! File.Exists(SaveFilePath))
                return;
            FileStream stream = new FileStream(SaveFilePath, FileMode.Open);
            BinaryFormatter formatter = new BinaryFormatter();
            _table = (List<ScoreRecording>) formatter.Deserialize(stream);
            stream.Close();
        }

        private void Start()
        {
            Load();
        }
    }

    [Serializable]
    public class ScoreRecording
    {
        [SerializeField] private string _playerName;
        [SerializeField] private int _score;

        public string PlayerName => _playerName;
        public int Score => _score;

        public ScoreRecording(string playerName, int score)
        {
            _playerName = playerName;
            _score = score;
        }
    }
}