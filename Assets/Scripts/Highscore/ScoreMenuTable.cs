﻿using System.Linq;
using DG.Tweening;
using UnityEngine;
using Zenject;

namespace Highscore
{
    public class ScoreMenuTable : MonoBehaviour
    {
        [SerializeField] private CanvasGroup _canvasGroup;
        [SerializeField] private GameObject _recordingPrefab;
        [SerializeField] private RectTransform _content;
        
        [Inject] private IHighscoreTableReadService HighscoreTable { get; set; }

        public void Show()
        {
            _canvasGroup.DOKill();
            _canvasGroup.DOFade(1, 0.2f);
            _canvasGroup.blocksRaycasts = true;
        }

        public void Hide()
        {
            _canvasGroup.DOKill();
            _canvasGroup.DOFade(0, 0.2f);
            _canvasGroup.blocksRaycasts = false;
        }
        
        private void Start()
        {
            ScoreRecording[] recordings = HighscoreTable.Table.ToArray();

            foreach (ScoreRecording recording in recordings)
            {
                ScoreRecordingView recordingIsntance = Instantiate(_recordingPrefab, _content).GetComponent<ScoreRecordingView>();
                recordingIsntance.Init(recording.PlayerName, recording.Score);
            }
        }
    }
}