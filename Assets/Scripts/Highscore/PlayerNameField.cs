﻿using UnityEngine;
using Zenject;

namespace Highscore
{
    public class PlayerNameField : MonoBehaviour
    {
        [Inject] private IHighscorePlayerNameSetService HighscoreTable { get; set; }

        public void ApplyName(string playerName) => HighscoreTable.PlayerName = playerName;
    }
}