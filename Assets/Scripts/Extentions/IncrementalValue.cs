﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace Extentions
{
    [Serializable]
    public struct IncrementalValue
    {
        [FormerlySerializedAs("_startingRate")] [SerializeField] private float _startingValue;
        [SerializeField] private float _increment;
        
        private float _value;

        public float Value
        {
            get
            {
                ResetRate();
                return _value;
            }
            private set
            {
                ResetRate();
                _value = value;
            }
        }

        public float StartingValue => _startingValue;
        
        public float InvertedValue => 1 / Value;

        public float InvertedStartingValue => 1 / _startingValue;

        public void Increase() => Value += _increment;

        private void ResetRate()
        {
            if (_value.Equals(0))
                _value = _startingValue;
        }
    }
}