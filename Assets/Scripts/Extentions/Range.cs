﻿using System;
using UnityEngine;

namespace Extentions
{
    [Serializable]
    public struct Range
    {
        [SerializeField] private float _min;
        [SerializeField] private float _max;

        public float Min => _min;
        public float Max => _max;
        
        public float Random => UnityEngine.Random.Range(Min, Max);

        public float Lerp(float t) => Mathf.Lerp(Min, Max, t);

        private void OnValidate()
        {
            if (Max < Min)
                _max = _min;
        }
    }
}