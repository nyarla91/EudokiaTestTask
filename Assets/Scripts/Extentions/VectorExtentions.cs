﻿using UnityEngine;

namespace Extentions
{
    public static class VectorExtentions
    {        
        public static Vector3 WithZ(this Vector2 vector, float newZ) => new Vector3(vector.x, vector.y, newZ);
        
        public static Vector3 WithX(this Vector3 vector, float newX) => new Vector3(newX, vector.y, vector.z);
 
        public static Vector3 WithY(this Vector3 vector, float newY) => new Vector3(vector.x, newY, vector.z);

        public static Vector3 WithZ(this Vector3 vector, float newZ) => new Vector3(vector.x, vector.y, newZ);

        public static Vector3 XYtoXZ(this Vector2 vector) => new Vector3(vector.x, 0, vector.y);

        public static Vector3 RandomPointInBounds(this Bounds bounds)
        {
            Vector3 max = bounds.max;
            Vector3 min = bounds.min;
            return new Vector3(Random.Range(min.x, max.x), Random.Range(min.y, max.y), Random.Range(min.z, max.z));
        }
    }
}